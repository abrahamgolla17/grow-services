package missions.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name="jmm_attachement_tbl")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class Attachments implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ATTACHMENT_ID")
	private String attachmentId;
	
	@Column(name="ATTACHMENT_URL")
	private String attachmentUrl;
	
	@Column(name="ATTACHMENT_THUMBNAIL_URL")
	private String attachmentThumnailUrl;
	
	@Column(name="CREATED_DATE")
	private long createdDateTime;
	
	@Column(name="UPDATED_DATE")
	private long updatedDateTime;
	
	@Column(name="CONTENT_TYPE")
	private String imageType;;
	
	@ManyToOne
	@JoinColumn(name="EVENT_ID")
	private Event event;
	
	public String getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}

	public String getAttachmentUrl() {
		return attachmentUrl;
	}

	public void setAttachmentUrl(String attachmentUrl) {
		this.attachmentUrl = attachmentUrl;
	}

	public long getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(long createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public long getUpdatedDateTime() {
		return updatedDateTime;
	}

	public void setUpdatedDateTime(long updatedDateTime) {
		this.updatedDateTime = updatedDateTime;
	}

	public String getAttachmentThumnailUrl() {
		return attachmentThumnailUrl;
	}

	public void setAttachmentThumnailUrl(String attachmentThumnailUrl) {
		this.attachmentThumnailUrl = attachmentThumnailUrl;
	}

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
}
