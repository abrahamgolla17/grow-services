package missions.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@Entity
@Table(name="jmm_event_join_questionnaire_tbl")
public class EventJoinQuestionnaire implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="EVENT_ID")
	private Event event;
	
	@Id
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="QUESTION_ID")
	private Questionnaire question;
	
	@Column(name="ANSWER")
	private String answer;
	
	@Column(name="CREATED_DTTM")
	private long createdDateTime;

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public Questionnaire getQuestion() {
		return question;
	}

	public void setQuestion(Questionnaire question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public long getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(long createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
}
