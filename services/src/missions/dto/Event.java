package missions.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_ABSENT)
@Entity
@Table(name="jmm_event_tbl")
@JsonIgnoreProperties(ignoreUnknown=true)
public class Event implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="EVENT_ID")
	private String eventId;
	
	@Column(name="REPORT_TYPE")
	private String reportType;
	
	@Column(name="AREA_NAME")
	private String areaName;
	
	@Column(name="STATE")
	private String state;
	
	@Column(name="PINCODE")
	private String pincode;
	
	@Column(name="EVENT_LANGUAGE")
	private String language;
	
	@Column(name="EVENT_LATITUDE")
	private String latitude;
	
	@Column(name="EVENT_LONGITUDE")
	private String longitude;

	@Column(name="EVENT_DATE")
	private long eventDate;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="USER_ID")
	private User user;
	
	@OneToMany(mappedBy="event",fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Attachments> attachments;
	
	@OneToMany(mappedBy="event",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	private List<EventJoinQuestionnaire> eventsQuestionAnswers;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public long getEventDate() {
		return eventDate;
	}

	public void setEventDate(long eventDate) {
		this.eventDate = eventDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Attachments> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<Attachments> attachments) {
		this.attachments = attachments;
	}

	public List<EventJoinQuestionnaire> getEventsQuestionAnswers() {
		return eventsQuestionAnswers;
	}

	public void setEventsQuestionAnswers(List<EventJoinQuestionnaire> eventsQuestionAnswers) {
		this.eventsQuestionAnswers = eventsQuestionAnswers;
	}
}
