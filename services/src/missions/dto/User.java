package missions.dto;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name="jmm_user_tbl")
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_ABSENT)
public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USER_ID")
	private String userId;
	
	@Column(name="USER_TITLE")
	private String userTitle;
	
	@Column(name="USER_NAME",unique=true)
	private String userName;
	
	@Column(name="USER_IMAGE")
	private String userImageUrl;
	
	@Column(name="EMAIL_ID",unique=true)
	private String emailId;
	
	@Column(name="MOBILE_NO",unique=true)
	private String mobileNumber;
	
	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="TRANSACTION_PIN")
	private String transactionPassword;
	
	@Column(name="EFFDT")
	private long effectiveDate;
	
	@Column(name="EFF_STATUS")
	private boolean enable;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Column(name="CREATED_DTTM")
	private long createdDateTime;
	
	@Column(name="LASTUPD_BY")
	private String lastUpdatedBy;
	
	@Column(name="LASTUPD_DTTM")
	private long lastUpdatedDateTime;
	
	private boolean defaultPassword;
	
	private String gender;
	
	private String firstName;
	
	private String lastName;
	
	@OneToMany(mappedBy="user",fetch=FetchType.LAZY)
	private Set<Church> churches;
	
	@OneToMany(mappedBy="user",fetch=FetchType.LAZY)
	private Set<UserAddress> addresses;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserTitle() {
		return userTitle;
	}

	public void setUserTitle(String userTitle) {
		this.userTitle = userTitle;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTransactionPassword() {
		return transactionPassword;
	}

	public void setTransactionPassword(String transactionPassword) {
		this.transactionPassword = transactionPassword;
	}

	public long getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(long effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(long createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public long getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}

	public void setLastUpdatedDateTime(long lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}

	public Set<Church> getChurches() {
		return churches;
	}

	public void setChurches(Set<Church> churches) {
		this.churches = churches;
	}

	public Set<UserAddress> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<UserAddress> addresses) {
		this.addresses = addresses;
	}

	public boolean isDefaultPassword() {
		return defaultPassword;
	}

	public void setDefaultPassword(boolean defaultPassword) {
		this.defaultPassword = defaultPassword;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
