package missions.dto.constants;

public enum UserTitles {
	MR,
	MS,
	MRS,
	DR,
	REV,
	BRO,
	FAT
}
