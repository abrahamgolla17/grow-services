package missions.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="jmm_church_tbl")
public class Church implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CHURCH_ID")
	private String churchId;
	
	@Column(name="CHURCH_NAME")
	private String churchName;
	
	@Column(name="CHURCH_ADDRESS1")
	private String churchAddressLine1;
	
	@Column(name="CHURCH_ADDRESS2")
	private String churchAddressLine2;

	@Column(name="CHURCH_CITY")
	private String city;
	
	@Column(name="CHURCH_STATE")
	private String state;
	
	@Column(name="CHURCH_PINCODE")
	private String pinCode;
	
	@Column(name="CHURCH_COUNTRY")
	private String country;
	
	@Column(name="CHURCH_COORDINATES_LATITUDE")
	private String latitude;
	
	@Column(name="CHURCH_COORDINATES_LONGITUDE")
	private String longitude;
	
	@Column(name="CHURCH_AFFILIATION")
	private String churchAffiliation;
	
	@Column(name="EFFDT")
	private long effectiveDate;
	
	@Column(name="EFF_STATUS")
	private boolean enable;
	
	@Column(name="CREATED_BY")
	private String createdBy;
	
	@Column(name="CREATED_DTTM")
	private long createdDateTime;
	
	@Column(name="LASTUPD_BY")
	private String lastUpdatedBy;
	
	@Column(name="LASTUPD_DTTM")
	private long lastUpdatedDateTime;
	
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private User user;

	public String getChurchId() {
		return churchId;
	}

	public void setChurchId(String churchId) {
		this.churchId = churchId;
	}

	public String getChurchName() {
		return churchName;
	}

	public void setChurchName(String churchName) {
		this.churchName = churchName;
	}

	public String getChurchAddressLine1() {
		return churchAddressLine1;
	}

	public void setChurchAddressLine1(String churchAddressLine1) {
		this.churchAddressLine1 = churchAddressLine1;
	}

	public String getChurchAddressLine2() {
		return churchAddressLine2;
	}

	public void setChurchAddressLine2(String churchAddressLine2) {
		this.churchAddressLine2 = churchAddressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getChurchAffiliation() {
		return churchAffiliation;
	}

	public void setChurchAffiliation(String churchAffiliation) {
		this.churchAffiliation = churchAffiliation;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(long effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(long createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public long getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}

	public void setLastUpdatedDateTime(long lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}
	
}
