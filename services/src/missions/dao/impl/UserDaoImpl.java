package missions.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import missions.dao.UserDao;
import missions.dto.User;
import missions.dto.UserAddress;

@Repository
public class UserDaoImpl implements UserDao{

	private Logger logger = Logger.getLogger(UserDaoImpl.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User getUserByName(String userName) {
		logger.info("Get user with name "+userName);
		try{
			CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<User> userQuery = cb.createQuery(User.class);
			Root<User> userRoot = userQuery.from(User.class);
			userQuery.where(cb.equal(userRoot.get("userName"), userName));
			userQuery.select(userRoot);

			return sessionFactory.getCurrentSession().createQuery(userQuery).getSingleResult();
		}catch(Exception e){
			e.getLocalizedMessage();
			return null;
		}
	}

	@Override
	public User registerUser(User user) {
		logger.info("Registering user with name "+user.getFirstName());
		try{
			sessionFactory.getCurrentSession().save(user);
			return user;
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException("Unable to save user details");
		}
	}

	@Override
	public boolean updateUser(User user) {
		logger.info("Updating user with id "+user.getUserId());
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		return true;
	}

	@Override
	public User getUserById(String userId) {
		logger.info("Get user with Id "+userId);
		try{
			CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<User> userQuery = cb.createQuery(User.class);
			Root<User> userRoot = userQuery.from(User.class);
			userQuery.where(cb.equal(userRoot.get("userId"), userId));
			userQuery.select(userRoot);

			return sessionFactory.getCurrentSession().createQuery(userQuery).getSingleResult();
		}catch(Exception e){
			e.getLocalizedMessage();
			return null;
		}
	}

	@Override
	public boolean save(UserAddress address) {
		logger.info("Updating user with id "+address.getAddressId());
		sessionFactory.getCurrentSession().saveOrUpdate(address);
		return true;
	}

	@Override
	public User getUserByMobile(String mobileNo) {
		logger.info("Get User by mobile no "+mobileNo);
		try{
			CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<User> userQuery = cb.createQuery(User.class);
			Root<User> userRoot = userQuery.from(User.class);
			userQuery.where(cb.equal(userRoot.get("mobileNumber"), mobileNo));
			userQuery.select(userRoot);

			return sessionFactory.getCurrentSession().createQuery(userQuery).getSingleResult();
		}catch(Exception e){
			e.getLocalizedMessage();
			return null;
		}
	}

	@Override
	public List<User> searchUser(String firstName, String lastName, String phNum, String city, String userId) {
		logger.info("Get user by first name "+firstName+" or last name "+ lastName+" or phone number "+phNum+" and city "+city);
		try{
			CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
			CriteriaQuery<User> userQuery = cb.createQuery(User.class);
			Root<User> userRoot = userQuery.from(User.class);
			Join<User, UserAddress> addresses = userRoot.join("addresses");
			if(phNum != null && !phNum.trim().isEmpty())
				userQuery.where(cb.equal(userRoot.get("mobileNumber"), phNum));
			if(firstName != null && !firstName.trim().isEmpty())
				userQuery.where(cb.like(userRoot.get("firstName"), firstName+"%"));
			if(lastName != null && !lastName.trim().isEmpty())
				userQuery.where(cb.like(userRoot.get("lastName"), lastName+"%"));
			if(city != null && !city.trim().isEmpty())
				userQuery.where(cb.equal(addresses.get("city"), city));
			if(userId != null && !userId.trim().isEmpty())
				userQuery.where(cb.equal(userRoot.get("userId"), userId));
			userQuery.select(userRoot);

			return sessionFactory.getCurrentSession().createQuery(userQuery).getResultList();
		}catch(Exception e){
			e.getLocalizedMessage();
			return null;
		}
	}

}
