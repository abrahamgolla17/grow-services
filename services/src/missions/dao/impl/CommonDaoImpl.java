package missions.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import missions.dao.CommonDao;
import missions.dto.Event;
import missions.dto.Questionnaire;

@Repository
public class CommonDaoImpl implements CommonDao{

	@Autowired
	private SessionFactory sessionFactory;


	private Logger logger = Logger.getLogger(CommonDaoImpl.class.getName());
	@Override
	public Questionnaire saveQuestion(Questionnaire question) {
		logger.info("Saving question with id"+question.getQuestionId());
		sessionFactory.getCurrentSession().save(question);
		return question;
	}
	@Override
	public List<Questionnaire> getQuestions() {
		logger.info("Get All Questions");
		CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Questionnaire> questionsQuery = cb.createQuery(Questionnaire.class);
		Root<Questionnaire> questions = questionsQuery.from(Questionnaire.class);
		questionsQuery.select(questions);
		return sessionFactory.getCurrentSession().createQuery(questionsQuery).getResultList();
	}
	@Override
	public Questionnaire getQuestionById(String questionId) {
		logger.info("Get question by id "+questionId);
		CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Questionnaire> questionsQuery = cb.createQuery(Questionnaire.class);
		Root<Questionnaire> questions = questionsQuery.from(Questionnaire.class);
		questionsQuery.where(cb.equal(questions.get("questionId"), questionId));
		questionsQuery.select(questions);

		return sessionFactory.getCurrentSession().createQuery(questionsQuery).getSingleResult();
	}
	@Override
	public List<Event> getStatsInAMonth(long from, long till) {
		
		CriteriaBuilder cb = sessionFactory.getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Event> eventQuery = cb.createQuery(Event.class);
		Root<Event> eventRoot = eventQuery.from(Event.class);
		if(from != 0 && till != 0)
			eventQuery.where(cb.between(eventRoot.get("eventDate"), from, till));
		
		return sessionFactory.getCurrentSession().createQuery(eventQuery).getResultList();
		
	}

}
