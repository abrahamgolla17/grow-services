package missions.dao;

import java.util.List;

import missions.dto.Event;
import missions.dto.Questionnaire;

public interface CommonDao {
	public Questionnaire saveQuestion(Questionnaire question);

	public List<Questionnaire> getQuestions();
	
	public Questionnaire getQuestionById(String questionId);

	public List<Event> getStatsInAMonth(long from, long till);
}
