package missions.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import missions.dao.CommonDao;
import missions.dao.EventDao;
import missions.dao.UserDao;
import missions.dto.Attachments;
import missions.dto.Event;
import missions.dto.EventJoinQuestionnaire;
import missions.dto.Questionnaire;
import missions.dto.User;
import missions.services.EventService;
import missions.services.util.Convert2Transient;
import missions.util.ProjectConfig;

@Service
@Transactional
public class EventServiceImpl implements EventService{

	private Logger logger = Logger.getLogger(EventServiceImpl.class.getName());

	@Autowired
	EventDao eventDao;

	@Autowired
	UserDao userDao;

	@Autowired
	CommonDao commonDao;

	Convert2Transient convert;

	EventServiceImpl(){
		convert = new Convert2Transient();
	}

	@Override
	public String createEvent(Event event, JSONArray attachments, JSONArray answers) {
		logger.info("Creating an Event");

		List<Attachments> attachmentsList = new ArrayList<>();
		ProjectConfig config = new ProjectConfig();

		event.setEventId(createId("EVENT"));

		if(attachments != null)
			for(int i = 0 ; i < attachments.length(); i++){
				JSONObject object = new JSONObject(attachments.get(i).toString());
				Attachments attachment = new Attachments();
				attachment.setAttachmentId(createId("ATTACHMENT")+i);
				attachment.setAttachmentUrl(config.getImagesContext()+"/"+event.getUser().getUserId()+"/"+event.getReportType()+"/"+object.getString("contentKey"));
				attachment.setCreatedDateTime(new Date().getTime());
				attachment.setEvent(event);
				eventDao.saveAttachment(attachment);
				attachmentsList.add(attachment);
			}

		event.setAttachments(attachmentsList);
		eventDao.createEvent(event);

		if(answers == null)
			throw new RuntimeException("Invalid Request");

		for(int i = 0; i < answers.length(); i++){
			JSONObject object = new JSONObject(answers.get(i).toString());
			EventJoinQuestionnaire answer = new EventJoinQuestionnaire();
			answer.setCreatedDateTime(new Date().getTime());
			answer.setEvent(event);
			answer.setQuestion(commonDao.getQuestionById(object.getString("questionId")));
			answer.setAnswer(object.getString("answer"));
			eventDao.save(answer);
		}

		return event.getEventId();
	}

	@Override
	public List<Event> getReportsofaUser(String userId, String reportType) {

		logger.info("Get reports of a user with user id "+userId);
		List<Event> allEventReports = new ArrayList<>(); 
		User user = userDao.getUserById(userId);

		if(user == null)
			throw new RuntimeException("Invalid user");

		List<Event> reports = eventDao.getReportsofaUser(user,reportType);
		if(reports != null)
			for(Event e : reports)
				allEventReports.add(convert.convert2Transient(e));

		return allEventReports;
	}

	private String createId(String value){
		long l = new Date().getTime();
		switch (value) {
		case "EVENT":
			return "EVENT"+l;
		case "ATTACHMENT":
			return "ATTCH"+l;
		default:
			break;
		}
		return null;
	}

	/*
	 * ALGO:
	 * 1. GET EVENTS WHICH ARE NULL
	 * 2. GET THE QUESTION NUMBER RELATED TO EACH QU
	 * 3. GET THE ANSWER TO THAT QUESTION NUMBER
	 * 4. SET IT TO EVENT
	 * */

	@Override
	public boolean updateAllEvents(){

		List<Event> events = eventDao.getAllEventsWithNulls();
		for(Event e : events){
			List<EventJoinQuestionnaire> ejqs = e.getEventsQuestionAnswers();
			for(EventJoinQuestionnaire ejq : ejqs){
				Questionnaire q = ejq.getQuestion();
				if(q.getQuestion().equalsIgnoreCase("state")){
					e.setState(ejq.getAnswer());
				}
				if(q.getQuestion().equalsIgnoreCase("Pin Code")){
					e.setPincode(ejq.getAnswer());
				}
				if(q.getQuestion().equalsIgnoreCase("Language")){
					e.setLanguage(ejq.getAnswer());
				}
				if(q.getQuestion().equalsIgnoreCase("Geo-Location")){
					if(ejq.getAnswer() != null){
						String[] coords = ejq.getAnswer().split(","); 
						if(coords.length > 1){
							e.setLatitude(coords[0]);
							e.setLongitude(coords[1]);
						}
					}

				}
			}
			eventDao.saveOrUpdate(e);
		}

		return false;
	}

	@Override
	public void deeleteAll(String userId) {
		User user = userDao.getUserById(userId);
		List<Event> userEvents = eventDao.getReportsofaUser(user, null);
		for(Event e : userEvents){
			eventDao.deleteEvents(e);
		}
		
		
	}
}
