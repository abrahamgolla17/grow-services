package missions.services.impl;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

import missions.dao.UserDao;
import missions.dto.User;
import missions.dto.UserAddress;
import missions.services.UserService;
import missions.services.util.Convert2Transient;
import missions.util.PasswordUtil;
import missions.util.ProjectConfig;

@Service
@Transactional
public class UserServicesImpl implements UserService{

	Convert2Transient convert;

	UserServicesImpl(){
		convert = new Convert2Transient();
	}

	@Autowired
	UserDao userDao;

	private Logger logger = Logger.getLogger(UserServicesImpl.class.getName());

	@Override
	public User validateUser(String mobileNo, String password, boolean console) {
		logger.info("validate user");
		User user = null;
		if(console)
			user = userDao.getUserByName(mobileNo);
		else
			user = userDao.getUserByMobile(mobileNo);
		if(user != null){
			try{
				if(PasswordUtil.check(password, user.getPassword()))
					return convert.convert2TransientNull(user);
			}catch(Exception e){
				e.printStackTrace();
				throw new RuntimeException(e.getLocalizedMessage());
			}
		}
		return null;
	}

	@Override
	public boolean createUser(User user, JSONArray addresses,Set<UserAddress> addrList) {
		logger.info("Create user");
		Set<UserAddress> addressList = new HashSet<>();
		try{
			int count = 0;
			if(user != null && user.getUserTitle().equals("SUPER")){
				logger.info("Creating super user");
				User superAdmin = userDao.getUserByName("superadmin");
				if(superAdmin == null){
					user.setCreatedDateTime(new Date().getTime());
					user.setDefaultPassword(true);
					user.setEnable(true);
					user.setPassword(PasswordUtil.getSaltedHash("123456"));
					user.setUserId(createId("USER")+count);	

					User pUser = userDao.registerUser(user);
					if(pUser != null)
						return true;
				}else
					logger.info("Super user already created");
			}else{
				logger.info("Creating user with name "+user.getUserName());
				user.setCreatedDateTime(new Date().getTime());
				user.setDefaultPassword(true);
				user.setEnable(true);
				user.setPassword(PasswordUtil.getSaltedHash("123456"));
				user.setUserId(createId("USER")+count);

				if(addrList != null) {
					logger.info("Adding user address");
					for(UserAddress add : addrList){
						UserAddress address = new UserAddress();
						address.setAddressId(createId("ADDRESS")+count);
						address.setUser(user);
						address.setAddressLine1(add.getAddressLine1());
						address.setAddressLine2(add.getAddressLine2());
						address.setAddressType(add.getAddressType());
						address.setCity(add.getCity());
						address.setCountry(add.getCountry());
						address.setCreatedBy(add.getCreatedBy());
						address.setCreatedDateTime(new Date().getTime());
						address.setEnable(add.isEnable());
						address.setLatitude(add.getLatitude());
						address.setLongitude(add.getLongitude());
						address.setPinCode(add.getPinCode());
						address.setState(add.getState());
						++count;
						userDao.save(address);
						addressList.add(address);

					}
				}else {
					for(int i = 0 ; i < addresses.length(); i++){
						ObjectMapper mapper = new ObjectMapper();
						UserAddress address = mapper.readValue(addresses.get(i).toString(), UserAddress.class);
						address.setAddressId(createId("ADDRESS")+i);
						address.setUser(user);
						userDao.save(address);
						addressList.add(address);
						++count;
					}
					++count;
				}

				user.setAddresses(addressList);
				User pUser = userDao.registerUser(user);
				if(pUser != null){
					logger.info("Adding image of the user");
					//Image to user path
					String imagePath = user.getUserImageUrl();
					if(imagePath != null){
						String imageLocation = saveImageAndGetLocation(imagePath,pUser.getUserId());
						pUser.setUserImageUrl(imageLocation);
						if(userDao.updateUser(pUser))
							return true;
						else
							return false;
					}else
						logger.info("There is no image");
					return true;
				}
				else 
					return false;
			}
		}catch(Exception e){
//			e.printStackTrace();
			System.out.println(e.getCause());
			System.out.println(e.getLocalizedMessage());
			return false;
		}
		return false;
	}

	private String saveImageAndGetLocation(String imagePath, String userId) {
		logger.info("Image path is "+imagePath+" for user "+userId);
		ProjectConfig config = new ProjectConfig();
		String IMAGES_BASE_PATH = config.getImagesPath();
		try{
			URI url = new URI(imagePath);
			String path = url.getPath();

			String imageName = path.substring(path.lastIndexOf("/")).replace("/", "");

			String oldImagePath = IMAGES_BASE_PATH+File.separator+path.substring(path.lastIndexOf("USER")).replace("/", File.separator);

			Path source = Paths.get(oldImagePath);

			long random = new Date().getTime();
			String newImagePath = IMAGES_BASE_PATH+File.separator+userId+File.separator+"profile"+File.separator+random+imageName;

			if(!new File(IMAGES_BASE_PATH+File.separator+userId+File.separator+"profile").exists())
				new File(IMAGES_BASE_PATH+File.separator+userId+File.separator+"profile").mkdirs();

			Path destination = Paths.get(newImagePath);

			Files.copy(source, destination);
			String newUri = config.getImageIpandPort()+"/"+userId+"/profile/"+random+imageName;
			URI updatedUrl = new URI(newUri);
			logger.info("Saving at "+updatedUrl.getPath());

			return updatedUrl.getPath();

		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException("Image Path is incorrect, please contact adminstrator");
		}
	}

	@Override
	public boolean createMultipleUsers(List<User> users) {
		if(users != null) {
			int i = 0;
			for(User user : users) {
				user.setUserId(createId("USER"));
				for(UserAddress address : user.getAddresses()) {
					address.setAddressId(createId("ADDRESS")+i);
					address.setUser(user);
					userDao.save(address);
					++i;
				}	
				User pUser = userDao.registerUser(user);
				if(pUser == null)
					return false;
			}
		}
		return true;
	}
	@Override
	public boolean updatePinorPassword(User user, JSONObject jsonObject, boolean isPin) {
		User registeredUser = userDao.getUserById(user.getUserId());
		if(isPin){
			logger.info("Updating pin "+ isPin);
			String pin = jsonObject.getString("newpin");
			String confirmPin = jsonObject.getString("confirmpin");
			if(pin.equals(confirmPin)){
				registeredUser.setTransactionPassword(confirmPin);
			}else
				throw new RuntimeException("Pin's did not match!!");
		}else{
			logger.info("Updating password "+ !isPin);
			String confirmPassword = jsonObject.getString("confirmpassword");
			String password = jsonObject.getString("newpassword");

			if(confirmPassword.equals(password)){
				try {
					registeredUser.setPassword(PasswordUtil.getSaltedHash(confirmPassword));
					registeredUser.setDefaultPassword(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else
				throw new RuntimeException("Passwords did not match");
		}

		registeredUser.setLastUpdatedDateTime(new Date().getTime());
		boolean result = userDao.updateUser(registeredUser);
		if(result)
			return true;
		return false;
	}

	@Override
	public List<User> searchUser(String firstName, String lastName, String phNum, String city, String userid) {
		logger.info("Search User based on first name "+firstName +", last name"+lastName+" mobile number "+phNum+" and "+city);

		List<User> userList = new ArrayList<>();
		List<User> users = userDao.searchUser(firstName,lastName, phNum, city, userid);

		if(users != null)
			for(User u : users)
				userList.add(convert.convert2TransientNull(u));
		return userList;
	}

	private String createId(String key) {
		long l = new Date().getTime();
		switch (key) {
		case "USER":
			return "USER"+l;
		case "ADDRESS":
			return "ADDR"+l;
		default:
			break;
		}
		return null;
	}

	@Override
	public boolean resetToDefaultPassword(String userId) {
		logger.info("Updating..");

		User registeredUser = userDao.getUserById(userId);

		if(registeredUser == null)
			throw new RuntimeException("Invalid user");
		try{
			registeredUser.setPassword(PasswordUtil.getSaltedHash("123456"));
			registeredUser.setDefaultPassword(true);
			registeredUser.setLastUpdatedDateTime(new Date().getTime());

			boolean result = userDao.updateUser(registeredUser);
			if(result)
				return true;
		}catch(Exception e){
			throw new RuntimeException("Password update failed, please contact adminstrator");
		}
		return false;
	}

	@Override
	public User getUserById(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateUser(JSONObject userObject) {
		logger.info("Updating user");

		if(userObject == null)
			throw new RuntimeException("Invalid data");

		if(!userObject.has("userId"))
			throw new RuntimeException("No valid user id");

		String userId = userObject.getString("userId");
		User user = userDao.getUserById(userId);
		user.setEmailId(userObject.getString("emailId"));
		user.setFirstName(userObject.getString("firstName"));
		user.setLastName(userObject.getString("lastName"));
		user.setGender(userObject.getString("gender"));
		user.setMobileNumber(userObject.getString("mobileNumber"));

		if(userObject.has("userImageUrl")){
			String imagePath = userObject.getString("userImageUrl");
			user.setUserImageUrl(saveImageAndGetLocation(imagePath, user.getUserId()));
		}

		Set<UserAddress> existingAddress = user.getAddresses();
		JSONArray userAddress  = userObject.getJSONArray("addresses");

		for(UserAddress ad : existingAddress){
			JSONObject addressObject =  new JSONObject(userAddress.get(0).toString());
			ad.setAddressLine1(addressObject.getString("addressLine1"));
			ad.setAddressLine2(addressObject.getString("addressLine2"));
			ad.setCity(addressObject.getString("city"));
			ad.setState(addressObject.getString("state"));
			ad.setPinCode(addressObject.getString("pinCode"));
			ad.setCountry(addressObject.getString("country"));
		}
		try{
			boolean result = userDao.updateUser(user);
			return result;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean saveImage(User user) {
		if(user != null){
			User existingUser = userDao.getUserById(user.getUserId());
			existingUser.setUserImageUrl(user.getUserImageUrl());
			try{
				boolean result = userDao.updateUser(existingUser);
				return result;
			}catch(Exception e){
				throw new RuntimeException("Something went wrong!!");
			}
		}

		return false;
	}

	@Override
	public User getUserByName(String username) {
		logger.info("Get user by name "+username);
		User user = userDao.getUserByName(username);
		return convert.convert2TransientNull(user);
	}

	@Override
	public boolean updateUser(User user) {
		logger.info("Updating user with id "+user.getUserId());
		user.setLastUpdatedDateTime(new Date().getTime());
		boolean updated = userDao.updateUser(user);
		return updated;
	}
}
