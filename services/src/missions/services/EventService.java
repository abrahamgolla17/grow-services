package missions.services;

import java.util.List;

import org.json.JSONArray;

import missions.dto.Event;

public interface EventService {
	public String createEvent(Event event, JSONArray attachments, JSONArray answers);

	public List<Event> getReportsofaUser(String userId, String reportType);
	
	public boolean updateAllEvents();
	
	public void deeleteAll(String userId);
}
