package missions.util;

import java.io.IOException;
import java.util.Properties;

public class ProjectConfig {

	private static Properties props = null;

	public ProjectConfig() {
		if ( props == null ) {
			synchronized(this) {
				if ( props == null ){
					props = new Properties();
					try {
						props.load(this.getClass().getClassLoader().getResourceAsStream("project.properties"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public String getImagesPath() {
		return getProperty("IMAGES_PATH");
	}

	public String getImageIpandPort(){
		String IP = getProperty("IMAGE_IP"); 
		String context = getProperty("IMAGE_CONTEXT");
		String PORT = getProperty("IMAGE_PORT"); 
		return IP+":"+PORT+"/"+context;
	}

	public String getImagesContext() {
		String context = getProperty("IMAGE_CONTEXT");
		return context;
	}

	private static String getProperty( String propertyName) {
		String propertyValue = props.getProperty(propertyName);
		if (propertyValue != null && (! propertyValue.isEmpty() ) ) {
			return propertyValue;
		}
		return null;
	}
}
