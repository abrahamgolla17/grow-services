package missions.resources.impl;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import missions.dto.Event;
import missions.dto.User;
import missions.resources.EventResource;
import missions.resources.util.Response;
import missions.services.EventService;
import missions.services.UserService;
import missions.util.PasswordUtil;

@RestController
public class EventResourceImpl implements EventResource{

	private Logger logger = Logger.getLogger(EventResourceImpl.class.getName());

	@Autowired
	UserService userService;

	@Autowired
	EventService eventService;

	@RequestMapping(path="/event", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> createEvent(
			@RequestHeader("Authorization") String authHeader,
			@RequestBody String eventDetails) {

		logger.info("creating event ");

		Response response = new Response();
		ObjectMapper mapper = new ObjectMapper();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}

		String decodedString = PasswordUtil.decodeBase64(authHeader);
		String[] userDetails = decodedString.split(":");
		String userName = userDetails[0];
		String password = userDetails[1];

		User user = userService.validateUser(userName, password,false);

		if(user == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		if(eventDetails != null){
			JSONObject eventObject = new JSONObject(eventDetails);
			JSONArray attachments = eventObject.getJSONArray("attachemts");
			JSONArray answersList = eventObject.getJSONArray("answers");
			try {
				Event event = mapper.readValue(eventDetails, Event.class);
				event.setUser(user);
				String eventId = eventService.createEvent(event, attachments, answersList);
				if(eventId != null){
					response.setData("Created Successfully");
					response.setResult(true);
				}
			} catch (IOException e) {
				e.printStackTrace();
				response.setData("something went wrong");
				response.setResult(false);
			}

		}

		return ResponseEntity.ok().body(response);
	}

	@CrossOrigin(origins="*")
	@RequestMapping(path="/reports", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> getReportsBasedOnUserId(
			@RequestHeader("Authorization") String authHeader,
			@RequestParam(name="user") String userId,
			@RequestParam(name="report", required=false) String reportType,
			@RequestParam(name="console", required=false,defaultValue="false") boolean console) {

		logger.info("Get reports of a user with id "+userId);

		Response response = new Response();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}
		User user = getValidUser(authHeader, console); 
		if(user == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		List<Event> events = eventService.getReportsofaUser(userId, reportType);
		/*if(reportType.equals("EVENT")){

		}else if(reportType.equals("MONTHLY")){
			eventService.getReportsofaUser(userId, reportType);
		}*/

		response.setData(events);
		response.setResult(true);

		return ResponseEntity.ok().body(response);
	}

	private User getValidUser(String authHeader, boolean console){

		logger.info("Validating the user");

		String decodedString = PasswordUtil.decodeBase64(authHeader);
		String[] userDetails = decodedString.split(":");
		try{
			String userName = userDetails[0];
			String password = userDetails[1];

			User user = userService.validateUser(userName, password,console);

			if(user != null)
				return user;
		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException("Invalid user details");
		}
		return null;
	}

	@RequestMapping(path="/test", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> test() {
		eventService.updateAllEvents();
		return null;
	}

	@CrossOrigin(origins="*")
	@RequestMapping(path="/allreports", method=RequestMethod.GET, produces=MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@Override
	public ResponseEntity<Response> getAllReports(
			@RequestHeader("Authorization") String authHeader,
			@RequestParam(name="report") String reportType,
			@RequestParam boolean console) {

		Response response = new Response();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}
		User user = getValidUser(authHeader, console); 
		if(user == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		//		eventService.getAllReportsByType(reportType);

		return null;
	}

	@RequestMapping(path="/delete/{userid}", method=RequestMethod.GET)
	@Override
	public ResponseEntity<Response> delete(
			@PathVariable String userid) {

		eventService.deeleteAll(userid);

		return null;
	}
}
