package missions.resources.impl;

import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import missions.dto.Questionnaire;
import missions.dto.User;
import missions.resources.CommonResource;
import missions.resources.util.Response;
import missions.services.CommonServices;
import missions.services.UserService;
import missions.util.ImageUtil;
import missions.util.PasswordUtil;

@RestController
public class CommonResourceImpl implements CommonResource{

	private Logger logger = Logger.getLogger(CommonResourceImpl.class.getName());

	@Autowired
	CommonServices commonServices;

	@Autowired
	UserService userService;

	@RequestMapping(path="/question/{operatingsystem}", produces=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.GET)
	@Override
	public ResponseEntity<Response> getQuestionaire(
			@PathVariable String operatingsystem, 
			@RequestParam String version) {

		logger.info("getQuestionaire ");
		Response response = new Response();

		List<Questionnaire> questions = commonServices.getAllQuestions();
		if(questions != null){
			response.setData(questions);
			response.setResult(true);
		}else{
			response.setData("Something went wrong");
			response.setResult(false);
		}


		return ResponseEntity.ok().body(response);
	}

	@RequestMapping(path="/question/{userid}", consumes=MediaType.APPLICATION_JSON_VALUE, method=RequestMethod.POST)
	@Override
	public ResponseEntity<Response> createQuesionaire(
			@PathVariable String userid, 
			@RequestBody String question) {
		logger.info("Create Questionaire ");
		Response response = new Response();

		ObjectMapper mapper = new ObjectMapper();

		try {
			Questionnaire questionObject = mapper.readValue(question, Questionnaire.class); 
			boolean result = commonServices.createQuestionaire(questionObject);
			if(result){
				response.setData("Added Successfully");
				response.setResult(true);
			}else{
				response.setData("Something went wrong");
				response.setResult(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ResponseEntity.ok().body(response);
	}

	@RequestMapping(path="/upload", method=RequestMethod.POST)
	@Override
	public ResponseEntity<Response> uploadMultipleImages(
			@RequestHeader("Authorization") String authHeader,
			@RequestParam(name="event", required=true) String eventType,
			MultipartFile[] files,
			HttpServletRequest request) {

		logger.info("/upload image "+files);

		logger.info("creating event ");

		Response response = new Response();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}

		if(files == null){
			response.setData("please upload valid files");
			response.setResult(false);
			return ResponseEntity.ok().body(response);
		}

		String decodedString = PasswordUtil.decodeBase64(authHeader);
		String[] userDetails = decodedString.split(":");
		String userName = userDetails[0];
		String password = userDetails[1];

		User user = userService.validateUser(userName, password,false);

		if(user == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		List<String> imageUrls = ImageUtil.saveMultipleImage(files,eventType, user.getUserId());
		response.setData(imageUrls);
		response.setResult(true);

		return ResponseEntity.ok().body(response);
	}

	@CrossOrigin(origins="*")
	@RequestMapping(path="/profile", method=RequestMethod.POST)
	@Override
	public ResponseEntity<Response> imageUpload(
			@RequestHeader("Authorization") String authHeader,
			@RequestParam(name="profile", required=true) String profilePic,
			MultipartFile files,
			HttpServletRequest request,
			@RequestParam(name="console", required=false,defaultValue="false") boolean console) {

		logger.info("/upload image "+files);
		Response response = new Response();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}
		User user = getValidUser(authHeader, console); 
		if(user == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		if(files == null){
			response.setData("please upload valid file");
			response.setResult(false);
			return ResponseEntity.ok().body(response);
		}
		try{
			String imageUrl = ImageUtil.saveImage(files,profilePic, user);
			if(imageUrl != null){
				URL url = new URL(imageUrl);
				user.setUserImageUrl(url.getPath());
				boolean updated = userService.saveImage(user);
				if(updated){
					response.setData(imageUrl);
					response.setResult(true);
				}else{
					response.setData("Something went wrong, please contact adminstrator");
					response.setResult(false);
				}
			}

		}catch(Exception e){
			e.printStackTrace();
			response.setData("Something went wrong!!");
			response.setResult(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

		return ResponseEntity.ok().body(response);
	}

	@CrossOrigin(origins="*")
	@RequestMapping(path="/stats", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> getStats(
			@RequestHeader("Authorization") String authHeader,
			@RequestParam(name="console", required=false,defaultValue="false") boolean console,
			@RequestParam(name="from",defaultValue="0",required=false) long from,
			@RequestParam(name="till",defaultValue="0",required=false) long till) {

		logger.info("/stats ");

		Response response = new Response();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}
		try{
			User user = getValidUser(authHeader, console); 
			if(user == null){
				response.setResult(false);
				response.setData("Unauthorized");
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
			}

			Map<String, Integer> stats = commonServices.getStatistics(from, till);
			response.setResult(true);
			response.setData(stats);
		}catch(Exception e){
			response.setData(e.getLocalizedMessage());
			response.setResult(false);
		}
		return ResponseEntity.ok().body(response);
	}

	private User getValidUser(String authHeader, boolean console){

		logger.info("Validating the user");

		String decodedString = PasswordUtil.decodeBase64(authHeader);
		String[] userDetails = decodedString.split(":");
		try{
			String userName = userDetails[0];
			String password = userDetails[1];
			User user = userService.validateUser(userName, password,console);

			if(user != null)
				return user;

		}catch(Exception e){
			throw new RuntimeException("Invalid user details");
		}


		return null;
	}



}
