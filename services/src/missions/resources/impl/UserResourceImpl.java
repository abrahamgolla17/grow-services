package missions.resources.impl;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

import missions.dto.User;
import missions.dto.UserAddress;
import missions.resources.UserResource;
import missions.resources.util.Response;
import missions.services.UserService;
import missions.util.PasswordUtil;

@RestController
public class UserResourceImpl implements UserResource {

	private Logger logger = Logger.getLogger(UserResourceImpl.class.getName());

	@Autowired
	UserService userService;

	@CrossOrigin(origins="*")
	@RequestMapping(path="/login", method=RequestMethod.POST)
	@Override
	public ResponseEntity<Response> login(
			@RequestHeader("Authorization") String authHeader,
			@RequestParam(name="console", required=false, defaultValue="false") boolean console) {
		Response response = new Response();
		System.out.println(authHeader);
		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}

		String decodedString = PasswordUtil.decodeBase64(authHeader);
		String[] userDetails = decodedString.split(":");
		String mobileNumber = userDetails[0];
		String password = userDetails[1];

		User user = userService.validateUser(mobileNumber, password, console);
		if(user != null){
			response.setData(user);
			response.setResult(true);
		}else{
			response.setData("Invalid username/password");
			response.setResult(false);
		}

		return ResponseEntity.ok().body(response);
	}

	@CrossOrigin(origins="*")
	@RequestMapping(path="/register", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> registerUser(
			@RequestHeader("Authorization") String authHeader,
			@RequestBody String user,
			@RequestParam(name="console",required=true,defaultValue="false") boolean console) {

		logger.info("Registering user");

		Response response = new Response();
		ObjectMapper mapper = new ObjectMapper();


		User superUser = getValidUser(authHeader, console); 
		if(superUser == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		try{
			JSONObject jsonObject = new JSONObject(user);

			User userObject = mapper.readValue(user, User.class);
			JSONArray addressList = jsonObject.getJSONArray("addresses");
			try{
				boolean result = userService.createUser(userObject, addressList,null);
				if(result){
					response.setData("Added successfully");
					response.setResult(true);
				}else{
					response.setData("Something went wrong");
					response.setResult(false);
				}	
			}catch(Exception e){
				response.setData(e.getLocalizedMessage());
				response.setResult(false);
			}

		}catch(Exception e){
			response.setData(e.getLocalizedMessage());
			response.setResult(false);
		}

		return ResponseEntity.ok().body(response);
	}

	@RequestMapping(path="/update", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> updatePasswordOrPin(
			@RequestHeader("Authorization") String authHeader,
			@RequestBody String passwordOrPinDetails, 
			@RequestParam(name="pin",required=true) boolean changePin) {

		logger.info("/update ");

		Response response = new Response();

		String decodedString = PasswordUtil.decodeBase64(authHeader);

		String[] userDetails = decodedString.split(":");
		String userName = userDetails[0];
		String password = userDetails[1];

		User user = userService.validateUser(userName, password, false);

		if(user != null){
			JSONObject jsonObject = new JSONObject(passwordOrPinDetails);
			try{
				if(!changePin){
					logger.info("Updating password");
					if(jsonObject != null && Pattern.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$*%^&+=])(?=\\S+$).{8,}$", jsonObject.getString("newpassword"))){
						if(jsonObject.getString("newpassword").equals(jsonObject.getString("confirmpassword"))){
							boolean result = userService.updatePinorPassword(user, jsonObject,false);
							if(result){
								response.setData("Updated successfully");
								response.setResult(result);
							}
						}else {
							response.setData("Passwords did not match");
							response.setResult(false);
						}
					}else{
						response.setData("Password should contain atleast 1 uppercase, 1 lowercase, 1 Special character");
						response.setResult(false);
					}
				}else{
					logger.info("Updating pin");
					if(jsonObject.getString("newpin").equals(jsonObject.getString("confirmpin")) && jsonObject.getString("newpin").length() == 4 ){
						boolean result = userService.updatePinorPassword(user,jsonObject,true);
						if(result){
							response.setData("Updated successfully");
							response.setResult(result);
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Caught error "+e.getMessage());
				response.setData("Updation failed");
				response.setResult(false);
			}

		}else{
			response.setData("Invalid username/password");
			response.setResult(false);
			ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		return ResponseEntity.ok().body(response);
	}

	@CrossOrigin(origins="*")
	@RequestMapping(path="/reset", method=RequestMethod.POST)
	@Override
	public ResponseEntity<Response> resetPasswordAndPin(
			@RequestHeader("Authorization") String authHeader, 
			@RequestParam(name="console",defaultValue="false") boolean console,
			@RequestParam(name="user",required=true) String userId) {

		logger.info("Reset ");

		Response response = new Response();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}
		User user = getValidUser(authHeader, console); 
		if(user == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		if(userId != null){
			try{
				boolean updated = userService.resetToDefaultPassword(userId);
				if(updated){
					response.setResult(true);
					response.setData("Updated Successfully");
				}else{
					response.setResult(false);
					response.setData("Updating failed");
				}	
			}catch(Exception e){
				response.setResult(false);
				response.setData(e.getLocalizedMessage());
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
			}
		}else{
			response.setResult(false);
			response.setData("Invalid data");
			return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT).body(response);
		}

		return ResponseEntity.ok().body(response);
	}

	@CrossOrigin(origins="*")
	@RequestMapping(path="/search", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> searchUser(
			@RequestHeader("Authorization") String authHeader,
			@RequestParam(name="first") String firstName,
			@RequestParam(name="last") String lastName,
			@RequestParam(name="ph") String phoneNumber,
			@RequestParam(name="city") String city,
			@RequestParam(name="user", required=false) String userid,
			@RequestParam(name="console", required=false,defaultValue="false") boolean console) {

		logger.info("Search user ");

		Response response = new Response();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}
		User user = getValidUser(authHeader, console); 
		if(user == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}
		try{
			List<User> users = userService.searchUser(firstName, lastName, phoneNumber, city, userid);
			if(users != null){
				response.setResult(true);
				response.setData(users);	
			}else{
				response.setResult(true);
				response.setData("No Result found");
			}
		}catch(Exception e){
			e.printStackTrace();
			response.setResult(false);
			response.setData("Something went wrong, please contact adminstrator");
		}
		return ResponseEntity.ok().body(response);
	}

	@SuppressWarnings("resource")
	@RequestMapping(path="/registerall", method=RequestMethod.POST)
	@Override
	public ResponseEntity<Response> addAllUsers(
			@RequestHeader("Authorization") String authHeader, 
			@RequestParam(name="type") String type, 
			MultipartFile file,
			HttpServletRequest request,
			@RequestParam(name="console", required=true, defaultValue="false") boolean console) {
		Response response = new Response();

		if(authHeader == null || authHeader.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}

		if(file == null){
			response.setData("please upload valid files");
			response.setResult(false);
			return ResponseEntity.ok().body(response);
		}


		User user = getValidUser(authHeader, console);

		if(user == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}

		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				File temp = File.createTempFile("users_list", ".csv");
				System.out.println("Temp file : " + temp.getAbsolutePath());

				//Get tempropary file path
				String absolutePath = temp.getAbsolutePath();
				String tempFilePath = absolutePath.
						substring(0,absolutePath.lastIndexOf(File.separator));

				System.out.println("Temp file path : " + tempFilePath);

				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(temp.getAbsolutePath()));
				stream.write(bytes);
				stream.close();
				String line = "";
				String cvsSplitBy = ",";
				BufferedReader br = new BufferedReader(new FileReader(temp.getAbsolutePath()));
				while ((line = br.readLine()) != null) {
					UserAddress address = new UserAddress();
					Set<UserAddress> addresses = new HashSet<>();
					User newUser = new User();
					// use comma as separator
					String[] usersList = line.split(cvsSplitBy);
					newUser.setUserTitle(usersList[0]);

					if(usersList[1] != null && usersList[5] != null && usersList[6] != null ) {
						//						String userName = usersList[1].trim();
						String firstName = usersList[5].trim();
						String lastName = usersList[6].trim();	
						newUser.setUserName(firstName+"_"+usersList[3].trim());
						newUser.setFirstName(firstName);
						newUser.setLastName(lastName);
					}


					newUser.setCreatedDateTime(new Date().getTime());
					newUser.setDefaultPassword(true);
					newUser.setEffectiveDate(new Date().getTime());
					//					newUser.setEmailId(usersList[2]);
					newUser.setEnable(true);
					newUser.setGender(usersList[4]);
					newUser.setMobileNumber(usersList[3]);
					newUser.setPassword(PasswordUtil.getSaltedHash("123456"));

					if(usersList[7] != null) {
						String addressLine1 = usersList[7].trim();
						address.setAddressLine1(addressLine1);
					}else {
						address.setAddressLine1("NA");
					}

					if(usersList[8] != null) {
						String addressLine2 = usersList[8].trim();
						address.setAddressLine2(addressLine2);
					}else {
						address.setAddressLine2("NA");
					}

					if(usersList[9] != null) {
						String city = usersList[9].trim();
						address.setCity(city);
					}else
						address.setCity("NA");

					if(usersList[10] != null) {
						address.setState(usersList[10].trim());
					}else
						address.setState("NA");

					if(usersList[11] != null) {
						address.setPinCode(usersList[11].trim());
					}else
						address.setPinCode(usersList[11]);

					address.setCountry(usersList[12]);
					address.setAddressType(usersList[13]);
					address.setCreatedDateTime(new Date().getTime());
					address.setEnable(true);
					addresses.add(address);
					newUser.setAddresses(addresses);
					boolean result = userService.createUser(newUser, null,addresses);
					if(!result)
						throw new RuntimeException("Something went wrong");
					else {
						response.setData("Added successfully");
						response.setResult(true);
					}

				}
				br.close();
			}catch(Exception e) {
				e.printStackTrace();
				response.setData(e.getLocalizedMessage());
				response.setResult(false);
			}
		}
		return ResponseEntity.ok().body(response);

	}

	@CrossOrigin(origins="*")
	@Override
	@RequestMapping(path="/admin/reset/{user}", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> resetAdminPassword(
			@PathVariable String user,
			@RequestBody String passwordDetails) {

		logger.info("/admin/reset/"+user);

		Response response = new Response();

		if(user != null && !user.trim().isEmpty()){
			User userObject = userService.getUserByName(user);
			if(userObject == null){
				response.setData("Invalid User Name");
				response.setResult(false);
			}else{
				try{
					JSONObject object = new JSONObject(passwordDetails);
					String newPwd = object.getString("newPassword");
					String confirmPwd = object.getString("confirmPassword");
					if(newPwd.equals(confirmPwd) && newPwd.length() > 0 ){
						userObject.setPassword(PasswordUtil.getSaltedHash(newPwd));
						boolean updated = userService.updateUser(userObject);
						if(updated){
							response.setData("Updated successfully!!");
							response.setResult(updated);
						}else{
							response.setData("Something went wrong please contact adminstrator");
							response.setResult(updated);
						}
					}else{
						response.setData("password mismatch");
						response.setResult(false);
					}
				}catch(Exception e){
					response.setData("Something went wrong!!");
					response.setResult(false);
				}

			}
		}else{
			response.setData("Invalid User");
			response.setResult(false);
		}


		return ResponseEntity.ok().body(response);
	}

	@CrossOrigin(origins="*")
	@RequestMapping(path="/edit", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
	@Override
	public ResponseEntity<Response> editUser(
			@RequestHeader("Authorization") String authorization,
			@RequestBody String pastorData, 
			@RequestParam boolean console,
			@RequestParam(name="user") String userId)
	{
		Response response = new Response();

		if(authorization == null || authorization.trim().isEmpty()){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.ok().body(response);
		}
		User superAdmin = getValidUser(authorization, console); 
		if(superAdmin == null){
			response.setResult(false);
			response.setData("Unauthorized");
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
		}
		if(pastorData != null){
			JSONObject userObject = new JSONObject(pastorData);
			if(userObject.has("userId")){
				try{
					boolean result = userService.updateUser(userObject);
					if(result){
						response.setResult(result);
						response.setData(result);
					}else{
						response.setResult(result);
						response.setData("Something went wrong");
					}
					return ResponseEntity.status(HttpStatus.OK).body(response);
				}catch(Exception e){
					e.printStackTrace();
					response.setResult(false);
					response.setData(e.getLocalizedMessage());
					return ResponseEntity.status(HttpStatus.OK).body(response);
				}
			}else{
				response.setResult(false);
				response.setData("Invalid user object");
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
			}
		}

		return null;
	}

	private User getValidUser(String authHeader, boolean console){

		logger.info("Validating the user");

		String decodedString = PasswordUtil.decodeBase64(authHeader);
		String[] userDetails = decodedString.split(":");
		try{
			String userName = userDetails[0];
			String password = userDetails[1];

			User user = userService.validateUser(userName, password,console);

			if(user != null)
				return user;

		}catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException("Invalid user details");
		}

		return null;
	}




}
