package missions.resources;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import missions.resources.util.Response;

public interface CommonResource {
	public ResponseEntity<Response> getQuestionaire(String osType, String version);
	public ResponseEntity<Response> createQuesionaire(String userid, String question); 
	public ResponseEntity<Response> uploadMultipleImages(String authHeader, String type,MultipartFile[] files, HttpServletRequest request);
	public ResponseEntity<Response> imageUpload(String authHeader, String type,MultipartFile file, HttpServletRequest request, boolean console);
	public ResponseEntity<Response> getStats(String authHeader, boolean console, long fromDate, long tillDate);
}
